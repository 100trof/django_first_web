from .models import Artiles, Category
from django.forms import ModelForm, TextInput, DateTimeInput, Textarea
from django import forms

choices = Category.objects.all().values_list('name', 'name')
choice_list = []

for item in choices:
    choice_list.append(item)

class ArticlesForm(ModelForm):
    class Meta:
        model = Artiles
        fields = ['title', 'category', 'author', 'full_text', 'date']

        widgets = {
            'title': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Заголовок'
            }),
            'category': forms.Select(choices=choice_list, attrs={
                'class': 'form-control',
                'placeholder': 'Категория'
            }),
            'author': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Автор'
            }),
            'full_text': Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Текст статьи'
            }),
            'date': DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'Дата публикации'
            })}
