from django.db import models
from django.urls import reverse

class Category(models.Model):
    name = models.CharField('Название категории', max_length=50)

    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('news_home')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Artiles(models.Model):
    title = models.CharField('Заголовок', max_length=50)
    category = models.CharField('Категория', max_length=30, default='Нет категории')
    author = models.CharField('Автор', max_length=30)
    full_text = models.TextField('Статья')
    date = models.DateTimeField('Дата публикации')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f'/news/{self.id}'

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

