from django.shortcuts import render, redirect
from .models import Artiles, Category
from .forms import ArticlesForm
from django.views.generic import DetailView, UpdateView, DeleteView, CreateView


def news_home(request):
    news = Artiles.objects.order_by('-date')
    return render(request, 'news/news_home.html', {'news': news})


class NewsDetailView(DetailView):
    model = Artiles
    template_name = 'news/details_view.html'
    context_object_name = 'article'


class NewsUpdateView(UpdateView):
    model = Artiles
    template_name = 'news/create.html'

    form_class = ArticlesForm


class NewsDeleteView(DeleteView):
    model = Artiles
    success_url = '/news/'
    template_name = 'news/news-delete.html'


def CategoryView(request, cats):
    category_el = Artiles.objects.filter(category=cats)
    return render(request, 'news/categories.html', {'cats':cats.title(), 'category_el':category_el})


def create(request):
    error = ''
    if request.method == 'POST':
        form = ArticlesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('news_home')
        else:
            error = 'Форма заполнена неверно!'

    form = ArticlesForm

    data = {'form': form,
            'error': error}

    return render(request, 'news/create.html', data)

class AddCategoryView(CreateView):
    model = Category
    template_name = 'news/add_category.html'
    fields = '__all__'
